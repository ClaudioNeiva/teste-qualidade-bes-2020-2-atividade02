package br.ucsal.bes20202.testequalidade.atividade02;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

public class Questao01VerificarEntradaTest {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	@Test
	public void testarNumeroNaFaixa() {
		// Dados de entrada
		ByteArrayInputStream inFake = new ByteArrayInputStream("78".getBytes());
		System.setIn(inFake);

		// Saída esperada
		Integer nEsperado = 78;

		// Executar o método e recuperar a saída atual
		Integer nAtual = Questao01.obterNumeroFaixa();

		// Comprar a saída esperada com a saída atual
		assertEquals(nEsperado, nAtual);
	}

	@Test
	public void testar2NumerosForaFaixa() {
		// Dados de entrada
		ByteArrayInputStream inFake = new ByteArrayInputStream("4500\n8000\n345".getBytes());
		System.setIn(inFake);

		// Saída esperada
		Integer nEsperado = 345;
		String mensagemEsperada = "Informe um número inteiro entre 1 e 1000 (intervalo fechado):" + LINE_SEPARATOR
				+ "Número fora da faixa." + LINE_SEPARATOR
				+ "Informe um número inteiro entre 1 e 1000 (intervalo fechado):" + LINE_SEPARATOR
				+ "Número fora da faixa." + LINE_SEPARATOR
				+ "Informe um número inteiro entre 1 e 1000 (intervalo fechado):" + LINE_SEPARATOR;

		// Executar o método e recuperar a saída atual
		PrintStream outOriginal = System.out;
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));
		Integer nAtual = Questao01.obterNumeroFaixa();
		String mensagemAtual = outFake.toString();
		System.setOut(outOriginal);

		// Comprar a saída esperada com a saída atual
		assertAll("Verificação da saída", 
				() -> assertEquals(mensagemEsperada, mensagemAtual),
				() -> assertEquals(nEsperado, nAtual));
	}

}
