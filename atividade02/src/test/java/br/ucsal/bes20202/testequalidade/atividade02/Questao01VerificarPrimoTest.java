package br.ucsal.bes20202.testequalidade.atividade02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Casos de teste
 * 
 * Entrada | Saída esperada
 * 
 * 1. n=7 | primo=true
 *
 * 2. n=18 | primo=false
 */
public class Questao01VerificarPrimoTest {

	@BeforeAll
	public static void setupClass() {
		System.out.println("setupClass()...");
	}

	@AfterAll
	public static void teardownClass() {
		System.out.println("teardownClass()...\n");
	}

	@BeforeEach
	public void setup() {
		System.out.println("	setup()...");
	}

	@AfterEach
	public void teardown() {
		System.out.println("	teardown()...\n");
	}

	@Test
	@DisplayName("Verificação que 7 é primo")
	public void testarVerificarPrimo7() {
		System.out.println("		testarVerificarPrimo7()...");

		// Dados entrada
		Integer n = 7;

		// Resultado esperado = true

		// Execução do método que está sendo testado e armazenamento do resultado atual
		Boolean ehPrimoAtual = Questao01.verificarPrimo(n);

		// Comparação do resultado esperado com o resultado atual
		assertTrue(ehPrimoAtual);
	}

	@Test
	@DisplayName("Verificação que o 18 não é primo")
	// @Disabled
	public void testarVerificarPrimo18() {
		System.out.println("		testarVerificarPrimo18()...");

		// Dados entrada
		Integer n = 18;

		// Resultado esperado = false

		// Execução do método que está sendo testado e armazenamento do resultado atual
		Boolean ehPrimoAtual = Questao01.verificarPrimo(n);

		// Comparação do resultado esperado com o resultado atual
		assertFalse(ehPrimoAtual);
	}

	@ParameterizedTest(name = "{index} verificarPrimo({0})={1}")
	@CsvSource({ "7,true", "8,false", "10,false", "20,false", "23,true", "35,false", "31,true", "37,true", "41,true",
			"90,false" })
	public void testar10Numeros(Integer n, Boolean ehPrimoEsperado) {
		System.out.println("testar " + n + "=" + ehPrimoEsperado);

		Boolean ehPrimoAtual = Questao01.verificarPrimo(n);

		assertEquals(ehPrimoEsperado, ehPrimoAtual);
	}

}
