package br.ucsal.bes20202.testequalidade.atividade02;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Casos de teste
 * 
 * 1. n=7, primo=true -> "O número 7 é primo"
 *
 * 2. n=8, primo=false -> "O número 8 NÃO é primo"
 *
 */
public class Questao01VerificarSaidaTest {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	private static PrintStream outOriginal;

	// Execute antes do primeiro método de teste executar
	@BeforeAll
	public static void setupClass() {
		outOriginal = System.out;
	}

	// Execute depois do último método de teste executar
	@AfterAll
	public static void teardownClass() {
		System.setOut(outOriginal);
	}

	@Test
	public void testarExibirPrimo() {
		// Dados de entrada
		Integer n = 7;
		Boolean isPrimo = true;

		// Saída esperada
		String mensagemEsperada = "O número 7 é primo" + LINE_SEPARATOR;

		// Executar o método e obter a saída atual
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));
		Questao01.exibirPrimoNaoPrimo(n, isPrimo);
		String mensagemAtual = outFake.toString();

		// Comparar a saída esperada com a saída atual
		Assertions.assertEquals(mensagemEsperada, mensagemAtual);
	}

	@Test
	public void testarExibirNaoPrimo() {
		// Dados de entrada
		Integer n = 8;
		Boolean isPrimo = false;

		// Saída esperada
		String mensagemEsperada = "O número 8 NÃO é primo" + LINE_SEPARATOR;

		// Executar o método e obter a saída atual
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));
		Questao01.exibirPrimoNaoPrimo(n, isPrimo);
		String mensagemAtual = outFake.toString();

		// Comparar a saída esperada com a saída atual
		Assertions.assertEquals(mensagemEsperada, mensagemAtual);
	}
}
